# Covid-19 vaccination model

### ETE modeling team

#### Bastien Reyné<sup>1∗</sup>, Quentin Richard<sup>1</sup>, Camille Noûs<sup>2</sup>, Christian Selinger<sup>1,3=</sup>, Mircea T. Sofonea<sup>1=</sup>, Ramsès Djidjou-Demasse<sup>1=</sup>, Samuel Alizon<sup>1,4=</sup>


1 : Laboratoire MIVEGEC (Univ. Montpellier, CNRS, IRD) — Montpellier, France  
2 : Laboratoire Cogitamus — https://www.cogitamus.fr/indexen.html  
3 : Swiss Tropical and Public Health Institute (Swiss TPH), CH-4002 — Basel, Switzerland  
4 : Center for Interdisciplinary Research in Biology (CIRB), College de France, CNRS, INSERM, Université PSL — Paris, France

* : corresponding author, `reyne.bastien_[AT]_gmail.com`

This repository provides the code used to compute the results of the study [Non-Markovian modelling highlights the importance of age structure on COVID-19 epidemiological dynamics](https://doi.org/10.1051/mmnp/2022008).



### Abstract

> The COVID-19 pandemic outbreak was followed by an huge amount of modeling studies in order to rapidly gain insights to implement the best public health policies. However, most of those compartmental models used a classical ordinary differential equations (ODEs) system based formalism that came with the tacit assumption the time spent in each compartment does not depend of the time already spent in it. To overcome this “memoryless” issue, a widely used workaround is to artificially increase and chain
the number of compartments of an unique reality (e.g. many compartments for infected individuals). It allows for a greater heterogeneity and thus be closer to the observed situation, at the cost of rendering the whole model more difficult to apprehend and parametrize. We propose here an alternative formalism based on a partial differential equations (PDEs) system instead of ordinary differential equations, which provides naturally a memory structure for each compartment, and thus allows to keep a restrained number of compartments.
We use such a model applied to the French situation, accounting for vaccinal and natural immunity. The results seem to indicate the vaccination rate is not enough to ensure the end of the epidemic, but, above all, highlight a huge uncertainty attributable to the age-structured contact matrix.


______________________________________________


### Instructions

All the model is implemented in `R` (v4.1.1, see `session_info.txt` file), and the following packages were used
`data.table`, `magrittr`, `Rcpp`, `doParallel`, `lhs`, `here`, `modelvacc`, `latex2exp`, `multisensi`, `sensitivity`, `tictoc`.

The `C++` parts were compiled with `g++` (v. 10.2.1) on Debian 12 (testing).

First, install the packages.

```r
## Install packages from CRAN
install.packages(c("Rcpp", "data.table", "magrittr", "doParallel", "lhs",
                   "here", "latex2exp", "multisensi", "sensitivity", "tictoc"))

## Install package containing model's Rcpp functions
library(here)
install.packages(here("ressources", "modelvacc_1.2.tar.gz"), repos = NULL)
```

**Note :** If you encounter issues with the `modelvacc` package, you might want to replace `source(modelvacc)` by `Rcpp::sourceCpp(here("ressources", "model_backend.cpp"))` in the `model.R` script.



Then, launching the scripts sequentially should be fine and produce the paper's results.




### Repository organisation

This repository is structured in two folders _scripts_ (providing the `R` scripts) and _ressources_ providing data and stuff used within the model.

**Note :** Most of the scripts are quite long to run (few hours up to few days), hence some intermediate results are available in order to run all the scripts.
The sensitivity analysis results are not hosted on this repository though, due to an high memory size (> 2 Go).

#### scripts

Script used:

* `1_fit_vaccination.R` 
* `2_fit_r0.R`
* `3_generate_lhs.R`
* `4_model_runs.R`
* `5_summarise_results.R`
* `6_sensitivity_analysis.R`
* `7_plot_results.R`

Script needeed by main scripts:

* `model.R`, containing the model's implementation and the parameters' baselines,
* `cadence_vaccination.R`, containing the vaccination rate function.

Supplementary scripts:

* `run_example.R`, illustratory script to show set up input parameters and retreive outputs,
* `contact_spf.R`, provided to show how were contructed the matrices (plus the paper's matrices plot),
* `fetch_data.R`, script used to fetch hospitalization and vaccination data.

#### ressources

* `modelvacc_1.2.tar.gz`, an `R` package containing the model' `Rcpp` functions,
* `model_backend.cpp`, the script providing the `Rcpp` functions,
* `vacc_params?Rdata`, containing results from first script,
* `control.Rdata`, containing the results from the second script fitting the contact rates,
* `contact_beraud.Rdata` containing the contact matrix from Béraud et al.,
* `contact_spf.Rdata` containing the 38 contact matrices from Santé Publique France,
* `contact_matrix.csv` containing the raw matrices from Santé Publique France,
* `hosp_true.Rdata` containing the hospitalization data usec within the study,
* `admin_doses.Rdata` and `admin_doses_age.Rdata` containing vaccination-related data used within this paper,
* `results.Rdata` contains the summarized dynamics from the 30400 runs used in the sensitivity analysis.

