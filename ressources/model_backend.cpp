#include <Rcpp.h>
using namespace Rcpp;

// Implementation of model equation.
// We tie each function with the equations from the paper that are dealt with.
// Overall, we used Euler explicit scheme for the implementation.

// [[Rcpp::export]]
double trapeze(const Rcpp::NumericVector X, const Rcpp::NumericVector Y) {   
  // It's the function fdapace::trapzRcpp() !
  double trapzsum = 0; 
  for (unsigned int ind = 0; ind != X.size() - 1; ++ind) {
    trapzsum += 0.5 * (X[ind + 1] - X[ind]) *(Y[ind] + Y[ind + 1]); 
  }  
  return trapzsum;
  
}


// [[Rcpp::export]]
NumericVector boucle_Imv(NumericVector oldImv,
                         int l_inf,
                         int l_vac,
                         NumericVector virul_reduc,
                         NumericVector infec_immun,
                         NumericVector prop_sever,
                         double lambd,
                         int age_c,
                         double stept,
                         double stepi,
                         double stepk,
                         NumericVector oldVk,
                         NumericVector mild_recov_rate) {
  // This function deals with Eq (6), (7), (14) from paper.
  
  NumericVector newImv(l_inf * l_vac);
  
  // Eq (7)
  IntegerVector indxk0 = {(Range(0, l_inf-1)) * l_vac}; 
  newImv[indxk0] = 0.0;
  
  for (int k = 1; k < l_vac; k++) {
    
    // Eq (6)
    newImv[k] = (oldImv[k] +
      stept / stepi * (1-infec_immun[k]) * (1 - (1 - virul_reduc[k]) * prop_sever[age_c-1]) * lambd * oldVk[k]) /
        (1.0 + stept / stepi + stept / stepk + stept * mild_recov_rate[0]); 
    
    for (int i = 1; i < l_inf; i++) {
      
      int ikcurr = i * l_vac + k;
      int iold = (i-1) * l_vac + k;
      int kold = i * l_vac + k-1;
      
      // Eq (14) using Euler explicit scheme
      newImv[ikcurr] = (oldImv[ikcurr] +
        stept / stepi * newImv[iold] +
        stept / stepk * newImv[kold]) /
          (1.0 + stept / stepi + stept / stepk + stept * mild_recov_rate[i]);
      
    }
    
  }
  
  
  return newImv;
}


// [[Rcpp::export]]
NumericVector intImv(int l_inf,
                     int l_vacc,
                     NumericVector Imv_curr,
                     NumericVector transm_reduc,
                     NumericVector vacc_immun) {
  // Compute the integral of (1 - \xi) * Imv over k
  // Needed for computing in Eq (15)
  
  NumericVector to_ret(l_inf);
  
  for (int i = 0; i < l_inf; i++) {
    
    IntegerVector indx = {Range(0, l_vacc-1) + l_vacc * i};
    NumericVector tmp = Imv_curr[indx];
    to_ret[i] = trapeze(vacc_immun, tmp * (1-transm_reduc));
    
  }
  
  return to_ret;
}


// [[Rcpp::export]]
NumericVector newRj(int l_nat,
                    int age_c,
                    NumericVector mild_recov_rate,
                    NumericVector sever_recov_rate,
                    NumericVector Isa, 
                    NumericVector Ima,
                    NumericVector infect,
                    NumericVector oldRj,
                    double stept,
                    double stepj,
                    double rho,
                    NumericVector natur_waning) {
  // This function deals with Eq (2), (12) from paper.
  // We use an Euler explicit scheme
  
  NumericVector new_Rj(l_nat);
  double intoldI = trapeze(infect, 
                           sever_recov_rate * Isa + mild_recov_rate * Ima);
  
  // Eq (2)
  new_Rj[0] = (oldRj[0] + stept / stepj * intoldI) /
    (1 + stept / stepj + stept * rho + stept * natur_waning[0]);
  
  // Eq (12)
  for (int j = 1; j < l_nat; j++) {
    
    new_Rj[j] = (oldRj[j] + stept / stepj * new_Rj[j-1]) /
      (1 + stept / stepj + stept * rho + stept * natur_waning[j]);
    
  }
  
  return new_Rj;
}


// [[Rcpp::export]]
NumericVector newVk(int l_vac,
                    NumericVector oldV,
                    double stept,
                    double stepk,
                    double rho,
                    int age_c,
                    double oldS,
                    NumericVector oldR,
                    NumericVector natur_immun,
                    NumericVector vac_waning,
                    int l_inf,
                    NumericVector mild_recov_rate,
                    NumericVector oldImv,
                    NumericVector infect,
                    NumericVector virul_reduc,
                    NumericVector infec_immun,
                    double prop_sever,
                    double lambd) {
  // Deals with Eq (3) and (13)
  // We use an explicit Euler scheme.
  
  NumericVector new_Vk(l_vac);
  
  double intR = trapeze(natur_immun, oldR);
  
  // Eq (3)
  new_Vk[0] = (oldV[0] + stept / stepk * rho * (oldS + intR)) /
    (1 + stept / stepk + stept * vac_waning[0] +
      stept * (1 - infec_immun[0]) * (1 - (1 - virul_reduc[0]) * prop_sever) * lambd +
      stept * (1 - infec_immun[0]) * (1 - virul_reduc[0]) * prop_sever * lambd);
  
  // Eq (13)
  for (int k = 1; k < l_vac; k++) {
    
    IntegerVector indx = {Range(0, l_inf-1) * l_vac + k};
    NumericVector tmp = oldImv[indx];
    double intimvrec = trapeze(infect, mild_recov_rate * tmp);
    
    new_Vk[k] = (oldV[k] + stept / stepk * new_Vk[k-1] + stept * intimvrec) /
      (1 + stept / stepk + stept * vac_waning[k] +
        stept * (1 - infec_immun[k]) * (1 - (1 - virul_reduc[k]) * prop_sever) * lambd +
        stept * (1 - infec_immun[k]) * (1 - virul_reduc[k]) * prop_sever * lambd);
    
  }
  
  return new_Vk;
}


// [[Rcpp::export]]
double newS(NumericVector oldR,
            NumericVector natur_waning,
            NumericVector natur_immun,
            NumericVector oldV,
            NumericVector vac_waning,
            NumericVector vacc_immun,
            double oldS, 
            double stept,
            double lambd,
            double rho,
            int age_c) {
  // Deal with Eq (8)
  // Use an explicit Euler scheme for implementation
  
  double int_recovered = trapeze(natur_immun, oldR * natur_waning);
  double int_vaccinated = trapeze(vacc_immun, oldV * vac_waning);
  
  double newS = (oldS + stept * int_recovered + stept * int_vaccinated) /
    (1 + stept * lambd + stept * rho);
  
  return newS;
  
}


// [[Rcpp::export]]
double newD(NumericVector infect,
            NumericVector mu_deads,
            NumericVector Id,
            double stept,
            double oldD) {
  // Compute the number of deceased over time
  // Not described in the paper
  
  double int_deads = trapeze(infect, mu_deads * Id);
  double res = oldD + stept * int_deads;
  
  return res;
}

// [[Rcpp::export]]
NumericMatrix newI(int l_inf,
                   NumericVector oldIm,
                   double stept,
                   double stepi,
                   NumericVector prop_sever,
                   int age_c,
                   double lambd,
                   double oldS,
                   NumericVector mild_recov_rate,
                   NumericVector sever_recov_rate,
                   NumericVector virul_reduc,
                   NumericVector infec_immun,
                   NumericVector oldV,
                   NumericVector vacc_immun,
                   NumericVector oldIs,
                   NumericVector oldId,
                   double ifrage,
                   NumericVector mu_deads) {
  // Deal with Eq (1), (4), (5), (9), (10), (11)
  
  NumericMatrix newInf(3, l_inf);
  // Line 0 : Eq (1) and (9)
  // Line 1 : Eq (4) and (10)
  // Line 2 : Eq (5) and (11)
  
  
  // Eq (1)
  newInf(0, 0) = (oldIm[0] + stept / stepi * (1 - prop_sever[age_c-1]) * lambd * oldS) /
    (1 + stept / stepi + stept * mild_recov_rate[0]);
  
  double intImvs = trapeze(vacc_immun, (1-virul_reduc) * (1 - infec_immun) * oldV);
  
  // Eq (4)
  newInf(1, 0) = (oldIs[0] + stept / stepi * prop_sever[age_c-1] * lambd * (oldS + intImvs) *
    (1 - ifrage / prop_sever[age_c-1])) /
      (1 + stept / stepi + stept * sever_recov_rate[0]);
  
  // Eq (5)
  newInf(2, 0) = (oldId[0] + stept / stepi * lambd * (oldS + intImvs) * ifrage) /
    (1 + stept / stepi + stept * mu_deads[0]);
  
  for (int i = 1; i < l_inf; i++) {
    
    // Eq (9)
    newInf(0, i) = (oldIm[i] + stept / stepi * newInf(0, i-1)) /
      (1 + stept / stepi + stept * mild_recov_rate[i]);
    
    // Eq (10)
    newInf(1, i) = (oldIs[i] + stept / stepi * newInf(1, i-1)) /
      (1 + stept / stepi + stept * sever_recov_rate[i]);
    
    // Eq (11)
    newInf(2, i) = (oldId[i] + stept / stepi * newInf(2, i-1)) /
      (1 + stept / stepi + stept * mu_deads[i]);
    
    
  }
  
  return newInf;
}

// [[Rcpp::export]]
double intImv2(int l_inf,
               int l_vacc,
               NumericVector Imv_curr,
               NumericVector vacc_immun,
               NumericVector infect) {
  // Compute Imv over i and k to know the number in this compartment each time step
  
  NumericVector to_ret(l_inf);
  
  for (int i = 0; i < l_inf; i++) {
    
    IntegerVector indx = {Range(0, l_vacc-1) + l_vacc * i};
    NumericVector tmp = Imv_curr[indx];
    to_ret[i] = trapeze(vacc_immun, tmp);
    
  }
  
  double to_ret2 = trapeze(infect, to_ret);
  
  return to_ret2;
}

