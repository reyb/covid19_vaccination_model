## Fitting the R0 and NPI scale parameters
## ETE modeling team, 2021

## Needeed to be run only if the parameters therein are changed
## /!\ Takes a lot of time (few hours)

# Preamble ----------------------------------------------------------------

## Load packages
library(magrittr)
library(data.table)
library(doParallel)
library(here)

## Load model
source(here("scripts", "model.R"))

## Load hospitalization data
load(here("ressources", "hosp_true.Rdata"))


# Parameters space --------------------------------------------------------

## Parameters space
b.ifr <- c(1.93, 1.37) ## variant related increase of virulence
b.rec <- c(0.132, 0.169) ## initial proportion of recovered
b.mgt <- c(1.75, 4.7) ## generation time Weibull scale parameter
b.sgt <- c(5.665, 5.665) ## generation time Weibull shape parameter
b.bet <- c(39, 39) ## select contact matrix from Béraud, et al.
b.tra <- c(0.475/0.5, 0.475/0.45) ## vaccintation and transmission parameter
b.inf <- c(0.625/0.7, 0.625/0.55) ## vaccintation and immunity parameter
b.vir <- c(0.8/0.85, 0.8/0.75) ## vaccintation and virulence parameter
b.dos <- c(4.5, 5e7) ## number of doses

bornes <- matrix(c(b.ifr, b.rec, b.mgt, b.sgt,
                   b.bet, b.tra, b.inf, b.vir, b.dos), 
                 ncol = 2,
                 byrow = TRUE)


# Run model for median parameter set --------------------------------------

model.r0 <- function(ctrl = controle_def, born = bornes) {
  
  ## Initial recovered population, used later
  init_susceptible_def <- 6.7e7
  init_recovered_def   <- 6.7e7

  ## Compute mean paramters
  params <- rowMeans(bornes)
  
  ## Transmission rates depending of the Weibulls parameters
  correct_gt <- function(recovery, mean_gt, sd_gt) {
    
    vapply(infectious_def,
           FUN = function(x) { dweibull(x, mean_gt, sd_gt) },
           FUN.VALUE = 0.1) /
      exp(-vapply(1:length(infectious_def),
                  FUN = function(x) { trapeze(infectious_def[1:x],
                                              recovery[1:x]) },
                  FUN.VALUE = 0.1))
    
  }
  
  ## Contact matrix 
  contacts <- if (params[5] < 39) { get(paste0("contact", floor(params[5]))) } else { beta_def }
  
  ## Vaccination properties sigmoids
  transmission_reduction_def <- vapply(vaccin_immunity_def,
                                       FUN = function(x) {
                                         if (x <= 28) { 0.475 * params[6] /
                                             (1 + exp((15-x)/2.75)) }
                                         else { (0.75-0.475) * params[6] /
                                             (1 + exp((54-x)/2)) +
                                             0.475 * params[6] / (1 + exp((15-28)/2.75)) }
                                       },
                                       FUN.VALUE = 0.1)
  infection_immunity_def <- vapply(vaccin_immunity_def,
                                   FUN = function(x) {
                                     if (x <= 28) { 0.625 * params[8] / (1 + exp((15-x)/3)) }
                                     else { (0.875-0.625) * params[8] / (1 + exp((54-x)/2)) +
                                         0.625 * params[8] / (1 + exp((15-28)/3)) }
                                   },
                                   FUN.VALUE = 0.1)
  virulence_intermediaire <- vapply(vaccin_immunity_def,
                                    FUN = function(x) {
                                      if (x <= 28) { 0.8 * params[7] / (1 + exp((15-x)/2.75)) }
                                      else { (0.925-0.8) * params[7] / (1 + exp((54-x)/2)) +
                                          0.8 * params[7] / (1 + exp((15-28)/2.75)) }
                                    },
                                    FUN.VALUE = 0.1)
  virulence_reduction_def <- (virulence_intermediaire - infection_immunity_def) / 
    (1 - infection_immunity_def)
  
  
  ## Vaccination
  vaccination_args = list(nb_vaccines = params[9], ## Vaccinal coverage reached
                          vaccine_weight = c(0, 0, cumprod(c(1, 2, 1.7, 3, 1, 5, 2))), 
                          target_age = rep(0.95, 9), 
                          t_vacc = time_space_def,
                          pop_init = init_pop_def,
                          age_prop = prop_age_def,
                          delay = 410-365)
  
  
  ## Initialisation parameters
  initialisation <- init_system(prop_severe = prop_severe_def * params[1],
                                ifr_age = ifr_age_def * params[1],
                                init_s = init_susceptible_def * (1 - params[2]),
                                init_r = init_recovered_def * params[2],
                                controle = ctrl,
                                mild_transmission = correct_gt(mild_recovery_rate_def, 
                                                               params[3], params[4]),
                                severe_transmission = correct_gt(severe_recovery_rate_def, 
                                                                 params[3], params[4]),
                                beta = contacts,
                                transmission_reduction = transmission_reduction_def,
                                virulence_reduction = virulence_reduction_def,
                                infection_immunity = infection_immunity_def,
                                args_vaccination = vaccination_args)
  
  ## Run
  return(execute_system(initialisation))
}



# Define objective --------------------------------------------------------

## Extract new hospitalisation dynamic
extract.dynamic.severes <- function(obj, chr = "count_H") {
  
  list2env(obj, envir = environment())

  debut <- 1:((410-365-12)/step_t+1)
  Reduce(`+`, mget(grep(chr, ls(), value = TRUE)))[-debut]
  
}

## Return part of the run wanted
dist.one.run <- function(nb_controle, ctrl) {
  
  
  ## Pre time plot
  if (nb_controle == 1) {
    
    run <- model.r0(ctrl)
    severes <- extract.dynamic.severes(run)[3]
  
    return(severes)
  }
  
  
  ## Plot time
  if (nb_controle > 1) {
    
    run <- model.r0(ctrl)
    severes  <- extract.dynamic.severes(run)[round(1:length(true_hosp)/0.3)]

    return(severes)
    
  }
  
}


# Fit procedure -----------------------------------------------------------

## Define time changes in R0/NPI
controle.time_def <- c(1-excedent+12, 1, 20, 40, 80, 100, 120, 150, 190, 250, 300) + excedent - 12

## Optimisation function
fit.median.r0 <- function(controle.time = controle.time_def) {

  ## Initialize output
  best.fit <- rep(1, length(controle.time))

  ## Time pre-plot
  controle.possiblo <- c(1, 1.01, 1.02, 1.03, 1.04, 1.05)
  cl <- makeCluster(detectCores() - 2) 
  registerDoParallel(cl)
  score.fit1 <- foreach(param_set = 1:6,
                        .combine = c,
                        .packages = c("Rcpp", "magrittr", "modelvacc", "tictoc"),
                        .export = ls(.GlobalEnv)) %dopar%
    {

      controle <- data.frame(time = controle.time,
                             R0   = rep(controle.possiblo[param_set],
                                        length(controle.time)))

      fit <- sum((true_hosp[1] - dist.one.run(1, controle))^2)
    }
  stopCluster(cl)

  ## Save results
  best.fit[1] <- controle.possiblo[which.min(score.fit1)]
  
  ## Other time periods
  for (time in 2:(length(controle.time)-1)) {
    
    ## Define R0/NPI possibilities
    ctr1 <- seq(0.85, 1.35, by = 0.05)
    ctr2 <- seq(1.5, 2, by = 0.05)
    controle.possiblo <- if (time == 9) { ctr2 } else { ctr1 }
    
    
    score.fit <- NULL
    cl <- makeCluster(detectCores() - 2) 
    registerDoParallel(cl)
    score.fit <- foreach(param_set = 1:length(controle.possiblo),
                         .combine = c,
                         .packages = c("Rcpp", "magrittr", "modelvacc", "tictoc"),
                         .export = ls(.GlobalEnv)) %dopar%
      {
        controle <- data.frame(time = controle.time,
                               R0   = c(best.fit[1:(time-1)],
                                        rep(controle.possiblo[param_set],
                                            length(controle.time) - time + 1)))
        
        indx <- controle.time[time]:controle.time[time+1] - excedent + 12
        fit <- sum((true_hosp[indx] - dist.one.run(2, controle)[indx])^2)
      }
    
    stopCluster(cl)
    
    ## Print and save results
    cat("Time =", time, "\n")
    cat(score.fit, "\n")
    best.fit[time] <- controle.possiblo[which.min(score.fit)]
    cat("Best fit =", best.fit, "\n")
    
  }
  
  ## Return the best fit
  return(best.fit)
  
} 




# Run ---------------------------------------------------------------------

## /!\ This part takes take few hours
npi.control <- fit.median.r0()
l.npi <- length(npi.control)
npi.control[l.npi] <- npi.control[l.npi-1]

controle_def <- rbind(time = controle.time_def,
                      R0 = npi.control)

save(controle_def, file = "control.Rdata")
## Report the results within the model.R script 
## control_def variable.
