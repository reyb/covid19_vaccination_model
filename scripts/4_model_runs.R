## Save model runs given an input parameter csv file
## ETE modeling team, 2021

## Should be launched from command line
## Example : Rscript 4_model_runs.R "lhs.csv" "runs" "1"

## The "1" parameters means it will compute the first hundred lines
## of the csv file, and hence compute and save 100 model runs

## /!\ This script might takes few minutes up to few days depending on 
## the number of runs made, the number of CPU available.
## Approximatively 3 min / run / CPUcore

# Preamble ----------------------------------------------------------------

## Load packages
library(doParallel)
library(here)

## Load model
source(here("scripts", "model.R"))

## Load contact matrices
load(here("ressources", "contact_spf.Rdata"))

## Input parameters
args <- commandArgs(trailingOnly = TRUE)
lhs <- read.csv(args[1])
folder <- args[2]
nb_array <- as.numeric(args[3])


# Run model with parameter combinations -----------------------------------

## Run model with changes being made
## This model runs during 3-4 minutes
model.sa <- function(latin, line, loc) {
  
  ## Initial conditions
  init_susceptible_def <- 6.7e7
  init_recovered_def   <- 6.7e7

  ## Read parameters
  params <- as.numeric(latin[line, ])
  
  ## Transmission rates depending of the Weibulls parameters
  correct_gt <- function(recovery, mean_gt, sd_gt) {
    
    vapply(infectious_def,
           FUN = function(x) { dweibull(x, mean_gt, sd_gt) },
           FUN.VALUE = 0.1) /
      exp(-vapply(1:length(infectious_def),
                  FUN = function(x) { trapeze(infectious_def[1:x],
                                              recovery[1:x]) },
                  FUN.VALUE = 0.1))
    
  }
  
  ## Contact matrix 
  contacts1 <- if (params[5] < 39) { get(paste0("contact", floor(params[5]))) } else { beta_def }
  contacts2 <- if (params[6] < 39) { get(paste0("contact", floor(params[6]))) } else { beta_def }
  contacts3 <- if (params[7] < 39) { get(paste0("contact", floor(params[7]))) } else { beta_def }
  contacts4 <- if (params[8] < 39) { get(paste0("contact", floor(params[8]))) } else { beta_def }
  contacts5 <- if (params[9] < 39) { get(paste0("contact", floor(params[9]))) } else { beta_def }
  contacts6 <- if (params[10] < 39) { get(paste0("contact", floor(params[10]))) } else { beta_def }
  contacts7 <- if (params[11] < 39) { get(paste0("contact", floor(params[11]))) } else { beta_def }
  contacts8 <- if (params[12] < 39) { get(paste0("contact", floor(params[12]))) } else { beta_def }
  contacts9 <- if (params[13] < 39) { get(paste0("contact", floor(params[13]))) } else { beta_def }
  
  ## Matrices normalization
  contacts1 <- contacts1 / sum(contacts1)
  contacts2 <- contacts2 / sum(contacts2)
  contacts3 <- contacts3 / sum(contacts3)
  contacts4 <- contacts4 / sum(contacts4)
  contacts5 <- contacts5 / sum(contacts5)
  contacts6 <- contacts6 / sum(contacts6)
  contacts7 <- contacts7 / sum(contacts7)
  contacts8 <- contacts8 / sum(contacts8)
  contacts9 <- contacts9 / sum(contacts9)
  
  ## Creating contact matrix
  contacts <- cbind(contacts1[, 1],
                    contacts2[, 2],
                    contacts3[, 3],
                    contacts4[, 4],
                    contacts5[, 5],
                    contacts6[, 6],
                    contacts7[, 7],
                    contacts8[, 8],
                    contacts9[, 9])
  
  
  ## Vaccination properties sigmoids
  transmission_reduction_def <- vapply(vaccin_immunity_def,
                                       FUN = function(x) {
                                         if (x <= 28) { 0.475 * params[14] / (1 + exp((15-x)/2.75)) }
                                         else { (0.75-0.475) * params[14] / (1 + exp((54-x)/2)) +
                                             0.475 * params[14] / (1 + exp((15-28)/2.75)) }
                                       },
                                       FUN.VALUE = 0.1)
  infection_immunity_def <- vapply(vaccin_immunity_def,
                                   FUN = function(x) {
                                     if (x <= 28) { 0.625 * params[15] / (1 + exp((15-x)/3)) }
                                     else { (0.875-0.625) * params[15] / (1 + exp((54-x)/2)) + # (0.875-0.625) * params[8]
                                         0.625 * params[15] / (1 + exp((15-28)/3)) }
                                   },
                                   FUN.VALUE = 0.1)
  virulence_intermediaire <- vapply(vaccin_immunity_def,
                                    FUN = function(x) {
                                      if (x <= 28) { 0.8 * params[16] / (1 + exp((15-x)/2.75)) }
                                      else { (0.925-0.8) * params[16] / (1 + exp((54-x)/2)) +
                                          0.8 * params[16] / (1 + exp((15-28)/2.75)) }
                                    },
                                    FUN.VALUE = 0.1)
  virulence_reduction_def <- 1 - (1-virulence_intermediaire)/(1-infection_immunity_def)
  
  ## Number of vaccinated
  vaccination_args <- args_vaccination_def
  vaccination_args$nb_vaccines <- params[17]
  
  
  ## Perform the different runs
  initialisation <- init_system(prop_severe = prop_severe_def * params[1],
                                ifr_age = ifr_age_def * params[1],
                                init_s = init_susceptible_def * (1 - params[2]),
                                init_r = init_recovered_def * params[2],
                                mild_transmission = correct_gt(mild_recovery_rate_def, params[3], params[4]),
                                severe_transmission = correct_gt(severe_recovery_rate_def, params[3], params[4]),
                                beta = contacts,
                                transmission_reduction = transmission_reduction_def,
                                virulence_reduction = virulence_reduction_def,
                                infection_immunity = infection_immunity_def,
                                args_vaccination = vaccination_args)
  
  
  
  assign(paste0("run", line + (nb_array - 1) * 100),
         execute_system(initialisation))
  
  ## Save the run
  params <- signif(params, 7)
  save(list = paste0("run", line + (nb_array - 1) * 100), file = paste0(folder,
                                                                        "/ifr=", params[1],
                                                                        "_rec=", params[2],
                                                                        "_mgt=", params[3],
                                                                        "_sgt=", params[4],
                                                                        "_be1=", params[5],
                                                                        "_be2=", params[6],
                                                                        "_be3=", params[7],
                                                                        "_be4=", params[8],
                                                                        "_be5=", params[9],
                                                                        "_be6=", params[10],
                                                                        "_be7=", params[11],
                                                                        "_be8=", params[12],
                                                                        "_be9=", params[13],
                                                                        "_tra=", params[14],
                                                                        "_inf=", params[15],
                                                                        "_vir=", params[16],
                                                                        "_dos=", params[17],
                                                                        ".Rdata"))
  
}




# Run ---------------------------------------------------------------------

## This is the part that might takes a lot of time
cl <- makeCluster(detectCores() - 2, outfile = "log.txt") # J'ai 8 <3
registerDoParallel(cl)
csv <- foreach(param_set = 1:nrow(lhs),
               .export = paste0("contact", 1:38),
               .packages = c("Rcpp", "modelvacc", "tictoc")) %dopar%
  {
    model.sa(lhs, param_set, "France")
  }


