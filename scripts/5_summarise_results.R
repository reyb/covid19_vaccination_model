## Extract the 0.025, 0.5 and 0.975 quantiles dynamics
## ETE modeling team, 2021

## Should launched from command line
## Rscript 5_summarise_results.R "lhs.csv" "runs" "results.Rdata"

## /!\ Needs a lot of RAM if a lot of runs were made (1-2 Mo per run)

# Preamble ----------------------------------------------------------------

args <- commandArgs(trailingOnly = TRUE)
lhs <- read.csv(args[1])
folder <- args[2]
results <- args[3]


# Summarise ---------------------------------------------------------------

## Load all runs
## Might takes time if a lot of runs were made
for (row in 1:nrow(lhs)) {
  params <- signif(as.numeric(lhs[row, ]), 7)
  load(paste0(folder,
              "/ifr=", params[1],
              "_rec=", params[2],
              "_mgt=", params[3],
              "_sgt=", params[4],
              "_be1=", params[5],
              "_be2=", params[6],
              "_be3=", params[7],
              "_be4=", params[8],
              "_be5=", params[9],
              "_be6=", params[10],
              "_be7=", params[11],
              "_be8=", params[12],
              "_be9=", params[13],
              "_tra=", params[14],
              "_inf=", params[15],
              "_vir=", params[16],
              "_dos=", params[17],
              ".Rdata"))
}


## Which is.na, debugging purpose
is.na.dynamic <- function(chr) {
  
  res <- do.call(rbind,
                 lapply(1:length(list.files(folder)),
                        function(x) {  extract.dynamic(get(paste0("run", x)), chr) } ))
  res2 <- apply(res, 2, is.na)
  res3 <- which(res2)
}

## Extract each dynamic
extract.dynamic <- function(obj, chr) {

  list2env(obj, envir = environment())

  debut <- 1:((410-365)/step_t+1)
  Reduce(`+`, mget(grep(chr, ls(), value = TRUE)))[-debut][1:1217]

}

## Return wanted dynamic range (2.5, 0.5, 0.975)
epidemic.dynamic <- function(chr) {

  res <- do.call(rbind,
                 lapply(1:length(list.files(folder)),
                        function(x) {  extract.dynamic(get(paste0("run", x)), chr) } ))
  res2 <- apply(res, 2, quantile, c(0.025, 0.5, 0.975))

}

## Sevre dynamics (12 days lag)
extract.dynamic.severes <- function(obj, chr) {

  list2env(obj, envir = environment())

  debut <- 1:((410-365-12)/step_t+1)
  Reduce(`+`, mget(grep(chr, ls(), value = TRUE)))[-debut][1:1217]

}

## Return wanted dynamic range (2.5, 0.5, 0.975)
epidemic.dynamic.severes <- function(chr) {

  res <- do.call(rbind,
                 lapply(1:length(list.files(folder)),
                        function(x) {  extract.dynamic.severes(get(paste0("run", x)), chr) } ))
  res2 <- apply(res, 2, quantile, c(0.025, 0.5, 0.975))

}

## Return dynamics for >= 70 y.o
extract.dynamic.vieux <- function(obj, chr) {

  list2env(obj, envir = environment())

  debut <- 1:((410-365-12)/step_t+1)
  Reduce(`+`, mget(grep(chr, ls(), value = TRUE)[c(7, 9)]))[-debut][1:1217]

}

epidemic.dynamic.vieux <- function(chr) {

  res <- do.call(rbind,
                 lapply(1:length(list.files(folder)),
                        function(x) {  extract.dynamic.vieux(get(paste0("run", x)), chr) } ))
  res2 <- apply(res, 2, quantile, c(0.025, 0.5, 0.975))

}


# Compute compartment dynamics --------------------------------------------

susceptibles  <- epidemic.dynamic("S_a")
vaccinated    <- epidemic.dynamic("count_V_a")
recovered     <- epidemic.dynamic("count_R_a")
infected      <- epidemic.dynamic("count_I")
mild.infected <- epidemic.dynamic("count_Imv")
severes       <- epidemic.dynamic.severes("count_H")
vieux         <- epidemic.dynamic.vieux("count_H")

## Save results
save(run1,
     susceptibles,
     vaccinated,
     recovered,
     infected,
     mild.infected,
     severes,
     vieux,
     file = paste0(results))
