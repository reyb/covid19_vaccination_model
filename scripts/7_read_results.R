## Plot some figure results
## ETE modeling team, 2021

# Preamble ----------------------------------------------------------------

## Load package
library(latex2exp)
library(here)
library(data.table)
library(magrittr)

## Load data & results
load(here("ressources", "results.Rdata"))
# load(here("ressources", "sa.Rdata")) ## NOT ON THE GIT (TOO HEAVY 2.1Go)
load(here("ressources", "admin_doses.Rdata"))
load(here("ressources", "hosp_true.Rdata"))


## Initial par
init.par <- par(no.readonly = TRUE)

## Compute NPI level efficacy
npi <- function(x) {
  R0 <- if (x[1] < 223) { 4.5 } else {6}
  return( signif((2 - sqrt(4 - 4 * (1 - x[2] / R0)))/2, 2) )
}


# New data ----------------------------------------------------------------

## Correspond to hosp_true.Rdata
hosp <- fread("https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c")
hosp2021 <- hosp[, c("dep", "jour", "incid_hosp")] %>%
 .[, .(hop = sum(incid_hosp)), by = jour] %>%
 .[jour >= "2020-12-26"]

true_hosp <- zoo::rollmean(hosp2021$hop,
                           k = 7,
                           align = "right",
                           fill = NA)[-(1:6)]#[1:485]


# Epidemic dynamics plot --------------------------------------------------

old.par <- par(no.readonly = TRUE)

debut <- 1:((410-365)/run1$step_t+1)
fin <- 1:(length(run1$time_space)-max(debut)+1)
x.axe <- run1$time_space[fin]
par(las = 1,
    family = "Latin Modern Roman",
    cex.lab = 1.15, cex.axis = 1.1,
    mar = c(2.1, 3.7, 2.8, 4.1))
plot(NA, 
     xlim = range(x.axe),
     ylim = range(susceptibles, vaccinated, recovered, infected, 6e7),
     ann = FALSE,
     axes = FALSE)

## Axis
indx <- round(quantile(1:length(x.axe),
                       cumsum(c(0, 32, 30, 32, 31, 32, 31, 32, 32, 31, 32, 31, 19))/365))
Sys.setlocale("LC_TIME", "C") 
axis.Date(1,
          at = as.Date(x.axe, origin = "1970-01-01")[indx],
          pos = 0,
          family = "EB Garamond 12",
          cex.axis = 1.2)
Sys.setlocale("LC_TIME", "fr_FR.UTF-8") 
abline(v = x.axe[length(x.axe)],
       col = "white",
       lwd = 2)

axis(2, pos = x.axe[1])
title(ylab = "Number of people",
      line = 2.6,
      family = "EB Garamond 12")

## Results
transparency <- 0.4
polygon(x = c(x.axe, rev(x.axe)),
        y = c(susceptibles[1, ], rev(susceptibles[3, ])),
        col = adjustcolor("dodgerblue", transparency),
        border = NA)
lines(x.axe, susceptibles[2, ], col = "dodgerblue")

polygon(x = c(x.axe, rev(x.axe)),
        y = c(vaccinated[1, ], rev(vaccinated[3, ])),
        col = adjustcolor("green4", transparency),
        border = NA)
lines(x.axe, vaccinated[2, ], col = "green4")

## True data
points(1:length(admin_doses$doses), admin_doses$doses, col = "darkgreen", pch = 19, cex = 0.5)
text(length(admin_doses$doses) + 18, admin_doses$doses[length(admin_doses$doses)],
     "True data", adj = 0.5, col = "darkgreen", xpd = TRUE,
     cex = 1.1, family = "EB Garamond 12")


polygon(x = c(x.axe, rev(x.axe)),
        y = c(recovered[1, ], rev(recovered[3, ])),
        col = adjustcolor("orange", transparency),
        border = NA)
lines(x.axe, recovered[2, ], col = "orange")

polygon(x = c(x.axe, rev(x.axe)),
        y = c(infected[1, ], rev(infected[3, ])),
        col = adjustcolor("purple", transparency),
        border = NA)
lines(x.axe, infected[2, ], col = "purple")


## R0
col.r0 <- "salmon3"
mtext(TeX("𝓡_0"), line = 1.4, adj = 0.0, cex = 1.3, col = col.r0)
segments(x0 = run1$controle$time[9] - 410 + 365,
         y0 = 0, y1 = 7e7,
         xpd = TRUE,
         lwd = 1.5, 
         lty = 4,
         col = col.r0)
mtext(c("4.5", "6"), line = 1.4, adj = 0.0, at = c(5, 181), cex = 1.1, col = col.r0)

## Control measures
col.ctr <- "slateblue3"
mtext(TeX("NPI efficacy"), line = 0.1, adj = 0, cex = 1.1, at = -45, col = col.ctr)
mtext(npi(run1$controle[2, ]), cex = 0.9, adj = 0.05, line = 0.2, col = col.ctr)
for (contr in 4:nrow(run1$controle)) {
  if (run1$controle$R0[contr] != run1$controle$R0[contr-1]) {
    mtext(npi(run1$controle[contr, ]),
          cex = 0.9,
          adj = 0,
          at = (run1$controle$time[contr]-410+367), 
          line = 0.2, 
          col = col.ctr)
    if (contr != 9) {
      segments(x0 = run1$controle$time[contr] -410 + 365,
               y0 = 0, y1 = 6.6e7,
               xpd = TRUE,
               lty = 4,
               col = col.ctr)
    }
  }
}

## Légende
ecart <- 0.2e7
text(x = 372, cex = 1.2, adj = 0, y = vaccinated[2, 1217], "Vaccinated", 
     col = "green4", xpd = TRUE, family = "EB Garamond 12")
text(x = 372, cex = 1.2, adj = 0, y = recovered[2, 1217] + ecart, "Recovered",
     col = "orange", xpd = TRUE, family = "EB Garamond 12")
text(x = 372, cex = 1.2, adj = 0, y = susceptibles[2, 1217] - ecart, "Susceptibles",
     col = "dodgerblue", xpd = TRUE, family = "EB Garamond 12")
text(x = 372, cex = 1.2, adj = 0, y = 0.1e7, "Infected",
     col = "purple", xpd = TRUE, family = "EB Garamond 12")
segments(365, -1e7, 365, 5e7, col = "white", lwd = 3)
par(old.par)


# Plot severes ------------------------------------------------------------

old.par <- par(no.readonly = TRUE)
debut <- 1:((410-365)/run1$step_t+1)
fin <- 1:(length(run1$time_space)-max(debut)+1)
x.axe <- run1$time_space[fin]
par(las = 1,
    family = "Latin Modern Roman",
    cex.lab = 1.2, 
    cex.axis = 1.1,
    mar = c(2.1, 3.1, 0, 0))
plot(NA, 
     xlim = range(x.axe),
     ylim = range(0, signif(severes, 1), 6500) * 1.11,
     ann = FALSE,
     axes = FALSE)

## Axis
indx <- round(quantile(1:length(x.axe),
                       cumsum(c(0, 32, 30, 32, 31, 32, 31, 32, 32, 31, 32, 31, 19))/365))
Sys.setlocale("LC_TIME", "C") 
axis.Date(1,
          at = as.Date(x.axe, origin = "1970-01-01")[indx],
          pos = 0,
          family = "EB Garamond 12",
          cex.axis = 1.2)
Sys.setlocale("LC_TIME", "fr_FR.UTF-8") 
abline(v = x.axe[length(x.axe)],
       col = "white",
       lwd = 2)
axis(2, pos = x.axe[1],
     at = seq(0, 6500, length.out = 6))
title(ylab = "New hospital admissions",
      line = 1.8,
      family = "EB Garamond 12")

## Results
transparency <- 0.2
polygon(x = c(x.axe, rev(x.axe)),
        y = c(severes[1, 1:1217], rev(severes[3, 1:1217])),
        col = adjustcolor("black", transparency),
        border = NA)
lines(x.axe, severes[2, 1:1217], col = "black", lwd = 1)

## R0
col.r0 <- "salmon3"
mtext(TeX("$𝓡_0$"), line = -1.4, adj = 0.0, cex = 1.3, col = col.r0)
segments(x0 = run1$controle$time[9] - 410 + 365,
         y0 = 0, y1 = 8000,
         xpd = FALSE,
         lwd = 1.5, 
         lty = 4,
         col = col.r0)
mtext(c("4.5", "6"), line = -1.3, adj = 0.0, at = c(5, 181), cex = 1.1, col = col.r0)

## Control measures
col.ctr <- "slateblue3"
mtext(TeX("NPI efficacy"), line = -2.65, adj = 0, cex = 1.1, at = -40, col = col.ctr)
mtext(npi(run1$controle[2, ]), cex = 1, adj = 0.05, line = -2.5, col = col.ctr)
for (contr in 4:nrow(run1$controle)) {
  if (run1$controle$R0[contr] != run1$controle$R0[contr-1]) {
    mtext(npi(run1$controle[contr, ]),
          cex = 1,
          adj = 0,
          at = (run1$controle$time[contr]-410+367), 
          line = -2.5, 
          col = col.ctr)
    if (contr != 9) {
      segments(x0 = run1$controle$time[contr] -410 + 365,
               y0 = 0, y1 = 7500,
               xpd = TRUE,
               lty = 4,
               col = col.ctr)
    }
  }
}


## True data
points(1:365,
       true_hosp[1:365],
       col = c(rep("royalblue3", 211), rep("tomato3", 365-211)), cex = 0.8, pch = 21,
       bg = adjustcolor(c(rep("royalblue3", 211), rep("tomato3", 365-211)), 0.4))
# text(211, true_hosp[211]+450,
#      "True data", adj = 0.5, col = "royalblue3", xpd = TRUE, cex = 1.1
#      , family = "EB Garamond 12")
segments(365, -1e7, 365, 6000, col = "white", lwd = 3, xpd = TRUE)


par(old.par)

